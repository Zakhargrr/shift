from sqlalchemy import Column, Integer, String

from database.sqlite import Base


class User(Base):
    __tablename__ = "users"

    id = Column(Integer, primary_key=True, index=True, )
    username = Column(String, nullable=False)
    password = Column(String, nullable=False)
    hashed_password = Column(String, default="")
    full_name = Column(String, nullable=False)
    current_salary = Column(String, nullable=False)
    next_promotion_date = Column(String, nullable=False)


"""
Строка password оставлена только для удобства проверки проекта. 
В реальном проекте в БД хранится только hashed_password
"""

from pydantic import BaseModel


class UserInfo(BaseModel):
    full_name: str
    current_salary: str
    next_promotion_date: str


class UserData(BaseModel):
    id: int
    username: str
    password: str
    full_name: str
    current_salary: str
    next_promotion_date: str

    class Config:
        orm_mode = True


class Token(BaseModel):
    access_token: str
    token_type: str


class TokenData(BaseModel):
    username: str

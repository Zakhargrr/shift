from fastapi import APIRouter, Depends
from passlib.context import CryptContext
from sqlalchemy.orm import Session

from database import schemas, models
from database.sqlite import engine, SessionLocal

models.Base.metadata.create_all(bind=engine)
pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")


def get_password_hash(password):
    return pwd_context.hash(password)


def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


router = APIRouter(
    prefix="/users",
    tags=["users"]
)


@router.post("/create", response_model=schemas.UserData)
def create_user(user: schemas.UserData, db: Session = Depends(get_db)):
    db_user = models.User(id=user.id,
                          username=user.username,
                          password=user.password,
                          hashed_password=get_password_hash(user.password),
                          full_name=user.full_name,
                          current_salary=user.current_salary,
                          next_promotion_date=user.next_promotion_date
                          )
    db.add(db_user)
    db.commit()
    db.refresh(db_user)
    return db_user


@router.delete("/delete/{id}")
def delete_user(id: str, db: Session = Depends(get_db)):
    db.query(models.User).filter(models.User.id == id).delete()
    db.commit()


@router.get("/current_user")
def get_current_user(username: str, db: Session = Depends(get_db)):
    result = db.query(models.User).filter(models.User.username == username).first()
    if not result:
        return None
    else:
        return result

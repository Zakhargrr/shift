from database.utils import router as database_router
from fastapi import FastAPI, Request
from fastapi.templating import Jinja2Templates
from auth.auth import router as auth_router
from fastapi.staticfiles import StaticFiles

app = FastAPI()
app.mount("/image", StaticFiles(directory="frontend/images"), name="image")
templates = Jinja2Templates(directory="frontend/templates")


@app.get("/")
def get_base_page(request: Request):
    return templates.TemplateResponse("index.html", {"request": request})


@app.get("/users/token")
def get_entry_page(request: Request):
    return templates.TemplateResponse("token.html", {"request": request})


@app.get("/users/data")
def get_entry_page(request: Request):
    return templates.TemplateResponse("data.html", {"request": request})


app.include_router(auth_router)
app.include_router(database_router)

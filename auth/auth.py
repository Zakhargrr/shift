from datetime import datetime, timedelta

from fastapi import Depends, HTTPException, status, APIRouter
from fastapi.security import OAuth2PasswordBearer, OAuth2PasswordRequestForm
from jose import jwt, JWTError
from sqlalchemy.orm import Session
from database import schemas, utils
from database.schemas import TokenData, Token
from database.utils import pwd_context

# для получения ключа использовалась команда openssl rand -hex 32 на сайте https://www.cryptool.org/en/cto/openssl
SECRET_KEY = "50115e3282ec31e98a54850879ac3bf6bb3bc69243ed3965a6256302b4afa8f4"
ALGORITHM = "HS256"

oauth2_scheme = OAuth2PasswordBearer(tokenUrl="auth/token")
router = APIRouter(
    prefix="/auth",
    tags=["auth"]
)


def verify_password(plain_password, hashed_password):
    return pwd_context.verify(plain_password, hashed_password)


def authenticate_user(username: str, password: str, db: Session = Depends(utils.get_db)):
    user = utils.get_current_user(username, db)
    if not user:
        return None
    if not verify_password(password, user.hashed_password):
        return None
    return user


def create_access_token(data: dict):
    to_encode = data.copy()
    expire = datetime.utcnow() + timedelta(minutes=15)
    to_encode.update({"exp": expire})
    encoded_jwt = jwt.encode(to_encode, SECRET_KEY, algorithm=ALGORITHM)
    return encoded_jwt


def get_user_with_token(token: str = Depends(oauth2_scheme), db: Session = Depends(utils.get_db)):
    credentials_exception = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Ошибка проверки учетных данных",
        headers={"WWW-Authenticate": "Bearer"},
    )
    try:
        payload = jwt.decode(token, SECRET_KEY, algorithms=[ALGORITHM])
        username = payload.get("sub")
        if username is None:
            raise credentials_exception
        token_data = TokenData(username=username)
    except JWTError:
        raise credentials_exception
    user = utils.get_current_user(token_data.username, db)
    if user is None:
        raise credentials_exception
    return user


@router.post("/token", response_model=Token)
def login(form_data: OAuth2PasswordRequestForm = Depends(), db: Session = Depends(utils.get_db)):
    user = authenticate_user(form_data.username, form_data.password, db)
    if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Неверный логин или пароль",
            headers={"WWW-Authenticate": "Bearer"},
        )
    access_token = create_access_token(
        data={"sub": user.username})
    return {"access_token": access_token, "token_type": "bearer"}


@router.get("/data")
def read_data(current_user: schemas.UserInfo = Depends(get_user_with_token)):
    return schemas.UserInfo(
        full_name=current_user.full_name,
        current_salary=current_user.current_salary,
        next_promotion_date=current_user.next_promotion_date
    )
